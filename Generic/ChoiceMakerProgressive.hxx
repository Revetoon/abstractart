//
// Created by revetoon on 2/18/20.
//

#include <algorithm>
#include <random>
#include <cassert>
#include <iomanip>
#include "ChoiceMakerProgressive.hh"

template<class T>
ChoiceMakerProgressive<T>::ChoiceMakerProgressive(std::initializer_list<Choice> list, int maxCallCount)
    : maxCalls(maxCallCount)
{
    for (const auto &c : list) {
        choices.push_back({
                                  idTracker,
                                  c.value,
                                  updateWeight(c.beginWeight, c.endWeight),
                                  c.beginWeight,
                                  c.endWeight,
                                  0.0
                          });
        ++idTracker;
    }
}

template<class T>
int ChoiceMakerProgressive<T>::addChoice(const ChoiceMakerProgressive::Choice &c) {
    choices.push_back({
                              idTracker,
                              c.value,
                              updateWeight(c.beginWeight, c.endWeight),
                              c.beginWeight,
                              c.endWeight,
                              0.0
                      });
    return idTracker++;
}

template<class T>
void ChoiceMakerProgressive<T>::setWeight(int id, double beginWeight, double endWeight) {
    choices[id].beginWeight = beginWeight;
    choices[id].endWeight = endWeight;
    choices[id].weight = updateWeight(beginWeight, endWeight);
}

template<class T>
T ChoiceMakerProgressive<T>::choose() {
    computeProbabilities();
    addCall();

    std::random_device dev;
    std::mt19937 rng(dev());
    static std::uniform_real_distribution<> rnd(0, 10);
    double val = rnd(rng) / 10.0;

    double probSum = 0.0;
    for (const auto &c : choices) {
        probSum += c.probability;
        if (val <= probSum)
            return c.value;
    }
    assert(false && "Should not happen since all probabilities should have been tested.");
}

template<class T>
void ChoiceMakerProgressive<T>::print(std::ostream &out) {
    computeProbabilities();
    out << "Choices: " << std::endl;
    out << "| ID    | Val   | Wgt   | BgWgt | EnWgt | prob  |" << std::endl;
    for (const auto &c : choices) {
        out <<  "| " << std::setfill(' ') << std::setw(5) << c.id \
            << " | " << std::setfill(' ') << std::setw(5) << c.value \
            << " | " << std::setfill(' ') << std::setw(5) << c.weight \
            << " | " << std::setfill(' ') << std::setw(5) << c.beginWeight \
            << " | " << std::setfill(' ') << std::setw(5) << c.endWeight \
            << " | " << std::setfill(' ') << std::setw(5) << c.probability << " |" << std::endl;
    }
}

template<class T>
const std::vector<typename ChoiceMakerProgressive<T>::ChoiceProba> &ChoiceMakerProgressive<T>::getChoices() {
    computeProbabilities();
    return choices;
}

template<class T>
void ChoiceMakerProgressive<T>::computeProbabilities() {
    double sum = 0;
    for (const auto &c : choices)
        sum += c.weight;

    for (auto &c : choices)
        c.probability = c.weight / sum;
}

template<class T>
void ChoiceMakerProgressive<T>::updateWeights() {
    for (auto &c : choices)
        c.weight = updateWeight(c.beginWeight, c.endWeight);
}

template<class T>
void ChoiceMakerProgressive<T>::addCall(int count) {
    callCount += count;
    updateTransition();
    updateWeights();
}

template<class T>
void ChoiceMakerProgressive<T>::updateTransition() {
    transition = std::clamp(static_cast<double>(callCount) / static_cast<double>(maxCalls), 0.0, 1.0);
}

template<class T>
inline double ChoiceMakerProgressive<T>::updateWeight(double b, double e) {
    return (1-transition) * b + transition * e;
}
