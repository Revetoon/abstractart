//
// Created by revetoon on 2/20/20.
//

#include "OpAvg.hh"

void OpAvg::build() {
    a = tracker.getOperator();
    b = tracker.getOperator();
}

double OpAvg::func() {
    double retVal = (a->func() + b->func()) / 2.0;
    assert(retVal >= -1 && retVal <= 1);
    return retVal;
}

void OpAvg::print(std::ostream &out) {
    out << "avg(";
    a->print(out);
    out << ", ";
    b->print(out);
    out << ")";
}

OpAvg::OpAvg(Tracker &tracker) : OpGeneric(tracker)
{
    build();
}
