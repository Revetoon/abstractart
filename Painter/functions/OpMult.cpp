//
// Created by revetoon on 2/17/20.
//

#include "OpMult.hh"

void OpMult::build() {
    a = tracker.getOperator();
    b = tracker.getOperator();
}

double OpMult::func() {
    double retVal = a->func() * b->func();
    assert(retVal >= -1 && retVal <= 1);
    return retVal;
}

void OpMult::print(std::ostream &out) {
    a->print(out);
    out << " * ";
    b->print(out);
}

OpMult::OpMult(Tracker &tracker) : OpGeneric(tracker)
{
    build();
}
