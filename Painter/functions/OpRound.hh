//
// Created by revetoon on 2/17/20.
//

#ifndef ABSTRACTART_OPROUND_HH
#define ABSTRACTART_OPROUND_HH


#include "OpGeneric.hh"

class OpRound : public OpGeneric {
public:
    OpRound(Tracker &tracker);

public:
    void build() override;

    double func() override;

    void print(std::ostream &out) override;

private:

    std::shared_ptr<OpGeneric> v;
    std::shared_ptr<OpGeneric> p;
};


#endif //ABSTRACTART_OPROUND_HH
