//
// Created by revetoon on 2/18/20.
//

#include "OpMod.hh"

void OpMod::build() {
    v = tracker.getOperator();
    m = tracker.getOperator();
}

double OpMod::func() {
    double val = m->func();
    if (val == 0)
        return 0;
    double retVal = fmod(v->func(), m->func());
    assert(retVal >= -1 && retVal <= 1);
    return retVal;
}

void OpMod::print(std::ostream &out) {
    out << "mod(";
    v->print(out);
    out << ", ";
    m->print(out);
    out << ")";
}

OpMod::OpMod(Tracker &tracker) : OpGeneric(tracker)
{
    build();
}
