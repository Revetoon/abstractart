//
// Created by revetoon on 2/18/20.
//

#ifndef ABSTRACTART_OPBELL_HH
#define ABSTRACTART_OPBELL_HH


#include "OpGeneric.hh"

class OpBell : public OpGeneric {
public:
    OpBell(Tracker &tracker);

public:
    void build() override;

    double func() override;

    void print(std::ostream &out) override;

private:
    std::shared_ptr<OpGeneric> v;
};


#endif //ABSTRACTART_OPBELL_HH
