//
// Created by revetoon on 2/17/20.
//

#ifndef ABSTRACTART_OPNEG_HH
#define ABSTRACTART_OPNEG_HH


#include "OpGeneric.hh"

class OpNeg : public OpGeneric {
public:
    OpNeg(Tracker &tracker);

public:
    void build() override;

    double func() override;

    void print(std::ostream &out) override;

private:

    std::shared_ptr<OpGeneric> v;

};


#endif //ABSTRACTART_OPNEG_HH
