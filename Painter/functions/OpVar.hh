//
// Created by revetoon on 2/17/20.
//

#ifndef ABSTRACTART_OPVAR_HH
#define ABSTRACTART_OPVAR_HH


#include "OpGeneric.hh"
#include "../Variable.hh"

class OpVar : public OpGeneric {
public:
    OpVar(Tracker &tracker);

    double func() override;

    void build() override;

    void print(std::ostream &out) override;

public:
    Variable *v;
};


#endif //ABSTRACTART_OPVAR_HH
