//
// Created by revetoon on 2/18/20.
//

#ifndef ABSTRACTART_CHOICEMAKERPROGRESSIVE_HH
#define ABSTRACTART_CHOICEMAKERPROGRESSIVE_HH

#include <vector>
#include <iostream>

template <class T>
class ChoiceMakerProgressive{
public:
    typedef struct Choice {
        T value;
        double beginWeight = 1.0;
        double endWeight = 0.0;
    } Choice;

    typedef struct ChoiceProba {
        int id;
        T value;
        double weight = 1.0;
        double beginWeight = 1.0;
        double endWeight = 0.0;
        double probability = 0.0;
    } ChoiceProba;

public:
    ChoiceMakerProgressive(std::initializer_list<Choice> list, int maxCallCount = 1);

    int addChoice(const Choice& c);

    void setWeight(int id, double beginWeight, double endWeight);

    T choose();

    void print(std::ostream &out);

    const std::vector<ChoiceProba> &getChoices();

    void addCall(int count = 1);

private:
    void updateTransition();

    void updateWeights();

    inline double updateWeight(double b, double e);

    void computeProbabilities();

private:
    int callCount = 0;
    int maxCalls;
    double transition = 0.0;

    int idTracker = 0;
    std::vector<ChoiceProba> choices;

};

#include "ChoiceMakerProgressive.hxx"

#endif //ABSTRACTART_CHOICEMAKERPROGRESSIVE_HH
