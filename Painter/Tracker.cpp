//
// Created by revetoon on 2/18/20.
//

#include "Tracker.hh"
#include "functions/Operators.hh"

#define shared_generic(OP) (std::shared_ptr<OpGeneric>(reinterpret_cast<OpGeneric*>(new OP)))

Tracker::Tracker(int maxRecurse, std::initializer_list<Variable *> vars)
     : maxRecurse(maxRecurse), cm(ChoiceMakerProgressive<OpName>({}, maxRecurse)), variables(vars)
{
    // 0 recurse
    cm.addChoice({OpName::OP_CONSTANT, 10, 1});
    cm.addChoice({OpName::OP_VAR , 10, 1});

//    // 1 recurse
//    cm.addChoice({OpName::OP_POW2 , 10, 0});
    cm.addChoice({OpName::OP_SIN , 10, 0});
//    cm.addChoice({OpName::OP_ABS , 10, 0});
    cm.addChoice({OpName::OP_COS , 10, 0});
    cm.addChoice({OpName::OP_NEG , 10, 0});
//    cm.addChoice({OpName::OP_POW3 , 10, 0});
    cm.addChoice({OpName::OP_BELL , 10, 0});
//~
//    // 2 recurse
    cm.addChoice({OpName::OP_ROUND , 10, 0});
//    cm.addChoice({OpName::OP_PLUS , 10, 0});
//    cm.addChoice({OpName::OP_MULT , 10, 0});
    cm.addChoice({OpName::OP_MOD , 10, 0});
    cm.addChoice({OpName::OP_AVG , 10, 0});
    cm.addChoice({OpName::OP_EUCLIDIAN , 10, 0});

//
//    // + recurse
//    cm.addChoice({OpName::OP_IFSUP , 10, 0});

    cm.print(std::cout); // TODO REMOVE
}

std::shared_ptr<OpGeneric> Tracker::    getOperator() {

    int val = cm.choose();

    switch (val) {

        // 0 recurse
        case OpName::OP_CONSTANT:
            return shared_generic(OpConstant(*this));
        case OpName::OP_VAR:
            return shared_generic(OpVar(*this));

        // 1 recurse
        case OpName::OP_POW2:
            return shared_generic(OpPow2(*this));
        case OpName::OP_SIN:
            return shared_generic(OpSin(*this));
        case OpName::OP_ABS:
            return shared_generic(OpAbs(*this));
        case OpName::OP_COS:
            return shared_generic(OpCos(*this));
        case OpName::OP_NEG:
            return shared_generic(OpNeg(*this));
        case OpName::OP_POW3:
            return shared_generic(OpPow3(*this));
        case OpName::OP_BELL:
            return shared_generic(OpBell(*this));

        // 2 recurse
        case OpName::OP_ROUND:
            return shared_generic(OpRound(*this));
        case OpName::OP_PLUS:
            return shared_generic(OpPlus(*this));
        case OpName::OP_MULT:
            return shared_generic(OpMult(*this));
        case OpName::OP_MOD:
            return shared_generic(OpMod(*this));
        case OpName::OP_AVG:
            return shared_generic(OpAvg(*this));
        case OpName::OP_EUCLIDIAN:
            return shared_generic(OpEuclidian(*this));

        // + recurse
        case OpName::OP_IFSUP:
            return shared_generic(OpIfSup(*this));
    }
    assert(false && "Switch statement should cover every cases.");
}

Variable *Tracker::getVariable() {
    std::random_device dev;
    std::mt19937 rng(dev());

    std::uniform_int_distribution<std::mt19937::result_type> rnd(0, variables.size() - 1);
    return variables[rnd(rng)];
}

double *Tracker::getConstant() {
    std::random_device dev;
    std::mt19937 rng(dev());

    static std::uniform_real_distribution<> rnd(-1, 1);

    double r = rnd(rng);
    double g = rnd(rng);
    double b = rnd(rng);

    constants.push_back(r);
    constantsR.push_back(r);
    constantsG.push_back(g);
    constantsB.push_back(b);

    return &constants.back();
}

void Tracker::randomizeConstants() {
    std::random_device dev;
    std::mt19937 rng(dev());

    static std::uniform_real_distribution<> rnd(-1, 1);

    size_t i = 0;

    for (i = 0; i < constants.size(); ++i) {
        constants[i] = rnd(rng);
    }

}
