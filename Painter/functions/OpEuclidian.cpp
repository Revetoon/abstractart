//
// Created by revetoon on 2/20/20.
//

#include "OpEuclidian.hh"

void OpEuclidian::build() {
    a = tracker.getOperator();
    b = tracker.getOperator();
}

double OpEuclidian::func() {
    double retVal = std::sqrt(pow(a->func(), 2) + pow(b->func(), 2)) / std::sqrt(2.000001);
    assert(retVal >= -1 && retVal <= 1);
    return retVal;
}

void OpEuclidian::print(std::ostream &out) {
    out << "euclidian(";
    a->print(out);
    out << ", ";
    b->print(out);
    out << ")";
}

OpEuclidian::OpEuclidian(Tracker &tracker) : OpGeneric(tracker)
{
    build();
}
