//
// Created by revetoon on 2/17/20.
//

#ifndef ABSTRACTART_SCREEN_HH
#define ABSTRACTART_SCREEN_HH


#include <vector>
#include "Color.hh"

#define SCREEN_DEFAULT_WIDTH            1600
#define SCREEN_DEFAULT_HEIGHT           800
#define SCREEN_DEFAULT_FILENAME         "out.jpg"

class Screen {


public:

    // Singleton
    static Screen & getInstance() {
        static Screen instance;

        return instance;
    }

private:
    Screen();
    Screen(int width, int height);

public:

    void init(int width, int height);

    void setPxl(int x, int y, const Color &c);
    void setPxlR(int x, int y, const double &r);
    void setPxlG(int x, int y, const double &g);
    void setPxlB(int x, int y, const double &b);
    Color getPxl(int x, int y) const;

    bool save( const char * path);


    Screen(Screen const&)               = delete;
    void operator=(Screen const&)       = delete;

private:

    Color *val(int x, int y);
    const Color *val(int x, int y) const;

    int width;
public:
    int getWidth() const;

    int getHeight() const;

    bool isInitialized() const;

private:
    int height;
    std::vector<Color> data;
    bool initialized;

};


#endif //ABSTRACTART_SCREEN_HH
