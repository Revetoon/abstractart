//
// Created by revetoon on 2/17/20.
//

#ifndef ABSTRACTART_OPABS_HH
#define ABSTRACTART_OPABS_HH


#include "OpGeneric.hh"

class OpAbs : public OpGeneric {
public:
    OpAbs(Tracker &tracker);

public:
    void build() override;

    double func() override;

    void print(std::ostream &out) override;

private:

    std::shared_ptr<OpGeneric> v;
};


#endif //ABSTRACTART_OPABS_HH
