//
// Created by revetoon on 2/17/20.
//

#include "OpIfSup.hh"

void OpIfSup::build() {
    a = tracker.getOperator();
    b = tracker.getOperator();
    yes = tracker.getOperator();
    no = tracker.getOperator();
}

double OpIfSup::func() {
    double va = a->func();
    double vb = b->func();
    if (va > vb) {
        double retVal = yes->func();
        assert(retVal >= -1 && retVal <= 1);
        return retVal;
    }
    else {
        double retVal = no->func();
        assert(retVal >= -1 && retVal <= 1);
        return retVal;
    }
}

void OpIfSup::print(std::ostream &out) {
    out << "if(";
    a->print(out);
    out << ", ";
    b->print(out);
    out << ", ";
    yes->print(out);
    out << ", ";
    no->print(out);
    out << ")";
}

OpIfSup::OpIfSup(Tracker &tracker) : OpGeneric(tracker)
{
    build();
}
