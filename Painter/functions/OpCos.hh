//
// Created by revetoon on 2/17/20.
//

#ifndef ABSTRACTART_OPCOS_HH
#define ABSTRACTART_OPCOS_HH


#include "OpGeneric.hh"

class OpCos : public OpGeneric {
public:
    OpCos(Tracker &tracker);

public:
    void build() override;

    double func() override;

    void print(std::ostream &out) override;

public:
    std::shared_ptr<OpGeneric> v;

};


#endif //ABSTRACTART_OPCOS_HH
