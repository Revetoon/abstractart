//
// Created by revetoon on 2/16/20.
//

#include "Color.hh"

Color::Color() {
}

Color::Color(int r, int g, int b)
    : r(static_cast<double>(r)/255.0), g(static_cast<double>(g)/255.0), b(static_cast<double>(b)/255.0)
{}

Color::Color(double r, double g, double b)
    : r(r), g(g), b(b)
{}

Color::Color(int r, int g, int b, int a)
    : r(static_cast<double>(r)/255.0), g(static_cast<double>(g)/255.0), b(static_cast<double>(b)/255.0),
      a(static_cast<double>(a)/255.0)
{}

Color::Color(double r, double g, double b, double a)
    : r(r), g(g), b(b), a(a)
{}


void Color::to256(int &rOut, int &gOut, int &bOut) {
    rOut = r * 255.0;
    gOut = g * 255.0;
    bOut = b * 255.0;
}

void Color::to256(int &rOut, int &gOut, int &bOut, int &aOut) {
    to256(rOut, gOut, bOut);
    aOut = a * 255.0;
}

Color::Color(int gray)
    : r(static_cast<double>(gray)/255.0), g(static_cast<double>(gray)/255.0), b(static_cast<double>(gray)/255.0)

{}

Color::Color(double gray)
    : r(gray), g(gray), b(gray)
{}
