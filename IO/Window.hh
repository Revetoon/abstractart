//
// Created by revetoon on 2/14/20.
//

#ifndef ABSTRACTART_WINDOW_HH
#define ABSTRACTART_WINDOW_HH

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <string>
#include <iostream>
#include <functional>
#include "../Generic/Color.hh"
#include "../Generic/Screen.hh"
#include "../External/Imgui/imgui.h"

#define WINDOW_DEFAULT_NAME         "Abstract Art"
#define WINDOW_DEFAULT_WIDTH        1600
#define WINDOW_DEFAULT_HEIGHT       800

class Window {
public:

    static Window & getInstance() {
        static Window instance;

        return instance;
    }

    Window();

private:
    Window(const std::string &windowName, int width, int height);

public:
    void init(const std::string &windowName, int width, int height);

    void loadPainting(Screen &s);

    void show();

    void forceQuitWindow();

    const Color &getBgCol() const;

    void setBgCol(const Color &c);

    void setMoving(bool moving);
    bool getMoving();

    void setSave(bool _save);

    bool isInitialized();

    Window(Window const&)               = delete;
    void operator=(Window const&)       = delete;

public:
    // event callbacks
    std::function<void(void)> KPSpace = []() { std::cout << "Undefined Key" << std::endl;};
    std::function<void(void)> KPEnter = []() { std::cout << "Undefined Key" << std::endl;};
    std::function<void(void)> KPConstants = []() { std::cout << "Undefined Key" << std::endl;};
    std::function<void(void)> KPMoving = []() { std::cout << "Undefined Key" << std::endl;};

private:
    //The window we'll be rendering to
    SDL_Window *window = nullptr;

    //The surface contained by the window
    SDL_Surface* screenSurface = nullptr; // TODO remove maybe

    SDL_Renderer* renderer = nullptr;

    // Imgui IO
    ImGuiIO io;

    // window name
    std::string windowName = "Window";

    // window screen dimension
    int width = 640;
    int height = 480;

    Color bgCol = Color::White();

    bool initialized;
    bool forceQuit;
    bool movingConstants;
    bool savePic;

};


#endif //ABSTRACTART_WINDOW_HH
