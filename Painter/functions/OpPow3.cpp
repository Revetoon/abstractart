//
// Created by revetoon on 2/18/20.
//

#include "OpPow3.hh"

double OpPow3::func() {
    double retVal = pow(v->func(), 3);
    assert(retVal >= -1 && retVal <= 1);
    return retVal;
}

void OpPow3::print(std::ostream &out) {
    out << "pow3(";
    v->print(out);
    out << ")";
}

void OpPow3::build() {
    v = tracker.getOperator();
}

OpPow3::OpPow3(Tracker &tracker) : OpGeneric(tracker)
{
    build();
}