//
// Created by revetoon on 2/17/20.
//

#include "OpCos.hh"

void OpCos::build() {
    v = tracker.getOperator();
}

double OpCos::func() {
    double retVal = cos(v->func());
    assert(retVal >= -1 && retVal <= 1);
    return retVal;
}

void OpCos::print(std::ostream &out) {
    out << "cos(";
    v->print(out);
    out << ")";
}

OpCos::OpCos(Tracker &tracker) : OpGeneric(tracker)
{
    build();
}
