
#include <iostream>
#include "IO/Window.hh"
#include "Painter/Painter.hh"
#include "Generic/Chronometer.hh"
#include "Properties.hh"

int main(int argc, char *argv[]) {

    std::cout << "Start" << std::endl;

    int width = 400;
    int height = 400;

    // paint
    Painter & p = Painter::getInstance();
    p.generateFormula(20);
    p.printFormula(std::cout);

    Screen & s = Screen::getInstance();
    s.init(width, height);
    {
        Chronometer chrono("Painting Time");
#ifdef PROGRAM_MULTITHREAD
        p.paintThreaded(s);
#else
        p.paint(s);
#endif // PROGRAM_MULTITHREAD
    }

    // DISPLAY
    {
        Window & w = Window::getInstance();
        w.init("Test", width, height);

        // setup callbacks
        w.KPSpace = []() {
            Screen::getInstance().save(SCREEN_DEFAULT_FILENAME);
            Window::getInstance().setSave(true);
            std::cout << "Saved to \"" << SCREEN_DEFAULT_FILENAME << "\"" << std::endl;
        };

        w.KPEnter = []() {
            std::cout << "Exiting" << std::endl;
            Window::getInstance().forceQuitWindow();
        };

        w.KPConstants = []() {
            std::cout << "Randomize Constants" << std::endl;
            Painter & p = Painter::getInstance();
            p.randomizeConstants();
            Screen & s = Screen::getInstance();
#ifdef PROGRAM_MULTITHREAD
            p.paintThreaded(s);
#else
            p.paint(s);
#endif // PROGRAM_MULTITHREAD
            Window & w = Window::getInstance();
            w.loadPainting(s);
        };

        w.KPMoving = []() {
            std::cout << "move" << std::endl;
            Window & w = Window::getInstance();
            w.setMoving(!w.getMoving());
        };

        w.loadPainting(s);
        w.show();

//        s.save("out.jpg");

    }

    return 0;
}