//
// Created by revetoon on 2/17/20.
//

#include "OpPow2.hh"

double OpPow2::func() {
    double retVal = pow(v->func(), 2);
    assert(retVal >= -1 && retVal <= 1);
    return retVal;
}

void OpPow2::print(std::ostream &out) {
    out << "pow2(";
    v->print(out);
    out << ")";
}

void OpPow2::build() {
    v = tracker.getOperator();
}

OpPow2::OpPow2(Tracker &tracker) : OpGeneric(tracker)
{
    build();
}
