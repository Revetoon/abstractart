//
// Created by revetoon on 2/17/20.
//

#ifndef ABSTRACTART_OPCONSTANT_HH
#define ABSTRACTART_OPCONSTANT_HH


#include "OpGeneric.hh"

class OpConstant : public OpGeneric {
public:
    OpConstant(Tracker &tracker);

    double func() override;

    void build() override;

    void print(std::ostream &out) override;

private:
    double *v = nullptr;
};


#endif //ABSTRACTART_OPCONSTANT_HH
