//
// Created by revetoon on 2/17/20.
//

#include "OpAbs.hh"

void OpAbs::build() {
    v = tracker.getOperator();
}

double OpAbs::func() {
    double retVal = fabs(v->func());
    assert(retVal >= -1 && retVal <= 1);
    return retVal;
}

void OpAbs::print(std::ostream &out) {
    out << "abs(";
    v->print(out);
    out << ")";
}

OpAbs::OpAbs(Tracker &tracker) : OpGeneric(tracker)
{
    build();
}
