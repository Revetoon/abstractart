//
// Created by revetoon on 2/17/20.
//

#ifndef ABSTRACTART_VARIABLE_HH
#define ABSTRACTART_VARIABLE_HH

#include <string>

typedef struct Variable {
    double value;
    std::string name;
} Variable;

#endif //ABSTRACTART_VARIABLE_HH
