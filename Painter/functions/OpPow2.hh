//
// Created by revetoon on 2/17/20.
//

#ifndef ABSTRACTART_OPPOW2_HH
#define ABSTRACTART_OPPOW2_HH


#include "OpGeneric.hh"

class OpPow2 : public OpGeneric {
public:
    OpPow2(Tracker &tracker);

    void build() override;

    double func() override;

    void print(std::ostream &out) override;

public:

    std::shared_ptr<OpGeneric> v = nullptr;
};


#endif //ABSTRACTART_OPPOW2_HH
