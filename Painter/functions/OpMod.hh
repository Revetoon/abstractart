//
// Created by revetoon on 2/18/20.
//

#ifndef ABSTRACTART_OPMOD_HH
#define ABSTRACTART_OPMOD_HH


#include "OpGeneric.hh"

class OpMod : public OpGeneric {
public:
    OpMod(Tracker &tracker);

public:
    void build() override;

    double func() override;

    void print(std::ostream &out) override;

public:
    std::shared_ptr<OpGeneric> v;
    std::shared_ptr<OpGeneric> m;
};


#endif //ABSTRACTART_OPMOD_HH
