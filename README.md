# Abstract Art

This project is an experiment to make "art" using mathematical functions.

## How it works

First of all, the program generates a random mathematical function like this
```
-avg(round(cos(Y), mod(avg(cos(cos(-X)), 0.478001), Y)), Y)
```

Then, for each pixel of the image and for each color channel (R, G and B), the function is run and the resulting float is converted into a value for the current color channel.

The X and Y are updated with the values of the current pixel and the constants have different values for each color channel.

For movements, we simply increment constants by epsilon each frame.

## Mathematical Functions

Every mathematical function **must** output a value in the range `[-1.0:1.0]`. Each function take a fixed number of input from 0 (for `Variable` and `Constant` functions) to up to 4 (for `IfSup` function).

- Variable : `X` and `Y` variables
- Constant : any random constant within `[-1.0:1.0]`
- Absolute
- Average
- Bell
- Cosinus
- Sinus
- Euclidian : `sqrt(a² + b²)`
- IfSup : `if (a > b) then c else d`
- Modulo
- Multiply
- Negate
- Plus
- Pow2
- Pow3
- Round

After running the function on one pixel for each color, we obtain 3 floats ranging from -1.0 to 1.0. Those values are then converted to a color to be displayed.



## Results

![https://giant.gfycat.com/DimpledEmotionalDeinonychus.webm](https://giant.gfycat.com/DimpledEmotionalDeinonychus.webm)

![https://giant.gfycat.com/SnoopyVibrantArmadillo.webm](https://giant.gfycat.com/SnoopyVibrantArmadillo.webm)

![https://giant.gfycat.com/CandidElegantAcaciarat.webm](https://giant.gfycat.com/CandidElegantAcaciarat.webm)

![https://giant.gfycat.com/ConcernedUnfitBlackfootedferret.webm](https://giant.gfycat.com/ConcernedUnfitBlackfootedferret.webm)

![https://giant.gfycat.com/PolishedValidFunnelweaverspider.webm](https://giant.gfycat.com/PolishedValidFunnelweaverspider.webm)

![https://giant.gfycat.com/DeadLiquidEasteuropeanshepherd.webm](https://giant.gfycat.com/DeadLiquidEasteuropeanshepherd.webm)