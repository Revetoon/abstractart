//
// Created by revetoon on 2/18/20.
//

#ifndef ABSTRACTART_CHOICEMAKER_HH
#define ABSTRACTART_CHOICEMAKER_HH

#include <iostream>
#include <vector>

template <class T>
class ChoiceMaker {
public:
    typedef struct Choice {
        T value;
        double weight = 1.0;
    } Choice;

    typedef struct ChoiceProba {
        int id;
        T value;
        double weight = 1.0;
        double probability = 0.0;
    } ChoiceProba;

public:
    ChoiceMaker(std::initializer_list<Choice> list);

    int addChoice(int value, double weight);
    int addChoice(const Choice& c);

    void setWeight(int id, double weight);

    T choose();

    void print(std::ostream &out);

    const std::vector<ChoiceProba> &getChoices();

private:
    void computeProbabilities();


private:
    int idTracker = 0;
    bool needRecompute = true;
    std::vector<ChoiceProba> choices;

};

#include "ChoiceMaker.hxx"

#endif //ABSTRACTART_CHOICEMAKER_HH
