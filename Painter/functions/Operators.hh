//
// Created by revetoon on 2/17/20.
//

#ifndef ABSTRACTART_OPERATORS_HH
#define ABSTRACTART_OPERATORS_HH

#include "OpConstant.hh"
#include "OpPow2.hh"
#include "OpPow3.hh"
#include "OpSin.hh"
#include "OpVar.hh"
#include "OpRound.hh"
#include "OpAbs.hh"
#include "OpCos.hh"
#include "OpNeg.hh"
#include "OpIfSup.hh"
#include "OpPlus.hh"
#include "OpMult.hh"
#include "OpMod.hh"
#include "OpBell.hh"
#include "OpAvg.hh"
#include "OpEuclidian.hh"


#endif //ABSTRACTART_OPERATORS_HH
