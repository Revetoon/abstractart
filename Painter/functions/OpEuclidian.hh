//
// Created by revetoon on 2/20/20.
//

#ifndef ABSTRACTART_OPEUCLIDIAN_HH
#define ABSTRACTART_OPEUCLIDIAN_HH


#include "OpGeneric.hh"

class OpEuclidian : public OpGeneric {
public:
    OpEuclidian(Tracker &tracker);

public:
    void build() override;

    double func() override;

    void print(std::ostream &out) override;

private:
    std::shared_ptr<OpGeneric> a;
    std::shared_ptr<OpGeneric> b;
};


#endif //ABSTRACTART_OPEUCLIDIAN_HH
