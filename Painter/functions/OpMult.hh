//
// Created by revetoon on 2/17/20.
//

#ifndef ABSTRACTART_OPMULT_HH
#define ABSTRACTART_OPMULT_HH


#include "OpGeneric.hh"

class OpMult : public OpGeneric {
public:
    OpMult(Tracker &tracker);

public:
    void build() override;

    double func() override;

    void print(std::ostream &out) override;

public:
    std::shared_ptr<OpGeneric> a;
    std::shared_ptr<OpGeneric> b;
};


#endif //ABSTRACTART_OPMULT_HH
