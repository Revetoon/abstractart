//
// Created by revetoon on 2/17/20.
//

#include "OpVar.hh"

double OpVar::func() {
    double retVal = v->value;
    assert(retVal >= -1 && retVal <= 1);
    return retVal;
}

void OpVar::print(std::ostream &out) {
    out << v->name;
}

void OpVar::build() {
    v = tracker.getVariable();
}

OpVar::OpVar(Tracker &tracker) : OpGeneric(tracker)
{
    build();
}
