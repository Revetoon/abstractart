//
// Created by revetoon on 2/18/20.
//

#include "OpBell.hh"

void OpBell::build() {
    v = tracker.getOperator();
}

double OpBell::func() {
    double retVal = (1.0 - 2.0 * pow(v->func(), 2));
    assert(retVal >= -1 && retVal <= 1);
    return retVal;
}

void OpBell::print(std::ostream &out) {
    out << "Bell(";
    v->print(out);
    out << ")";
}

OpBell::OpBell(Tracker &tracker) : OpGeneric(tracker)
{
    build();
}
