//
// Created by revetoon on 2/14/20.
//

#include "Window.hh"
#include "../External/Gl3w/gl3w.h"
#include "../External/Imgui/imgui.h"
#include "../External/Imgui/imgui_impl_sdl.h"
#include "../External/Imgui/imgui_impl_opengl3.h"
#include "../Painter/Painter.hh"
#include <cstdio>

//#define ENABLE_IMGUI


Window::Window()
    : windowName(WINDOW_DEFAULT_NAME), width(WINDOW_DEFAULT_WIDTH), height(WINDOW_DEFAULT_HEIGHT), initialized(false), forceQuit(false), movingConstants(false), savePic(false)
{
}

Window::Window(const std::string &windowName, int width, int height)
    : windowName(windowName), width(width), height(height), initialized(false), forceQuit(false), movingConstants(false), savePic(false)
{
    init(windowName, width, height);
}

void Window::init(const std::string &_windowName, int _width, int _height) {

    initialized = true;
    width = _width;
    height = _height;
    windowName = _windowName;

#ifdef ENABLE_IMGUI
    // GL 3.0 + GLSL 130
    const char* glsl_version = "#version 130";
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, 0);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
#endif //ENABLE_IMGUI

    //Create window
    window = SDL_CreateWindow( windowName.c_str(), SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, SDL_WINDOW_SHOWN | SDL_WINDOW_BORDERLESS);
    if( window == nullptr )
        throw std::runtime_error("Window could not be created! SDL_Error: \n" + std::string(SDL_GetError()));

#ifdef ENABLE_IMGUI
    //Create GL context for window
    SDL_GLContext gl_context = SDL_GL_CreateContext(window);
    SDL_GL_MakeCurrent(window, gl_context);
    SDL_GL_SetSwapInterval(1); // Enable vsync
#endif //ENABLE_IMGUI

    //Create renderer for window
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    if (renderer == nullptr)
        throw std::runtime_error( "Renderer could not be created! SDL Error: \n" + std::string(SDL_GetError()));


    // BG Color
    int r, g, b, a;
    bgCol.to256(r, g, b, a);

    //Initialize renderer color
    SDL_SetRenderDrawColor(renderer, r, g, b, a);

    // Initialize OpenGL loader
    bool err = gl3wInit() != 0;
    if (err)
    {
        throw std::runtime_error("Gl3w could not be initialized !");
    }

#ifdef ENABLE_IMGUI
    // Setup Dear ImGui context
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    io = ImGui::GetIO();
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;     // Enable Keyboard Controls
    //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls

    // Setup Dear ImGui style
    ImGui::StyleColorsDark();
    //ImGui::StyleColorsClassic();

    // Setup Platform/Renderer bindings
    ImGui_ImplSDL2_InitForOpenGL(window, gl_context);
    ImGui_ImplOpenGL3_Init(glsl_version);
#endif // ENABLE_IMGUI

    //Clear screen
//    SDL_RenderClear( renderer );

}


void Window::show() {

    static unsigned long long framesCount = 0;

    bool quit = false;
    forceQuit = false;
    bool showDemoWidow = false;

    //Event handler
    SDL_Event e;

    while (!quit && !forceQuit) {

#ifdef ENABLE_IMGUI
        // Start the Dear ImGui frame
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplSDL2_NewFrame(window);
        ImGui::NewFrame();
#endif // ENABLE_IMGUI

        //Handle events on queue
        while( SDL_PollEvent( &e ) != 0 )
        {
            //User requests quit
            if( e.type == SDL_QUIT )
            {
                quit = true;
            }
            else if (e.type == SDL_KEYUP)
            {
                switch (e.key.keysym.sym) {
                    case SDLK_SPACE:
                        KPSpace();
                        break;
                    case SDLK_RETURN:
                        KPEnter();
                        break;
                    case SDLK_c:
                        KPConstants();
                        break;
                    case SDLK_m:
                        KPMoving();
                        break;
                }
            }
        }

        //render 1 frame
        SDL_RenderClear(renderer);

        if (movingConstants) {
            Painter & p = Painter::getInstance();
            p.incrementConstants(0.01);
            Screen & s = Screen::getInstance();
            p.paint(s);
            Window & w = Window::getInstance();
            w.loadPainting(s);
            if (savePic) {
                std::stringstream ss;
                ss << "movie_" << framesCount;
                Screen::getInstance().save(ss.str().c_str());
            }

        }

        loadPainting(Screen::getInstance());
        SDL_RenderPresent(renderer);

        // Rendering
//        SDL_RenderClear(renderer);
//        SDL_RenderCopy(renderer, bitmapTex, NULL, NULL);
//        SDL_RenderPresent(renderer);

//        SDL_RenderClear(renderer);

#ifdef ENABLE_IMGUI
        // 1. Show the big demo window (Most of the sample code is in ImGui::ShowDemoWindow()! You can browse its code to learn more about Dear ImGui!).
        ImGui::ShowDemoWindow(&showDemoWidow);

        // Update Renderer
        SDL_RenderPresent(renderer );

        // Rendering
        ImGui::Render();
        glViewport(0, 0, (int)io.DisplaySize.x, (int)io.DisplaySize.y);
//        glClearColor(bgCol.r, bgCol.g, bgCol.b, bgCol.a);
        glClear(GL_COLOR_BUFFER_BIT);
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
//        SDL_RenderPresent(renderer );
        SDL_GL_SwapWindow(window);
#endif // ENABLE_IMGUI

        ++framesCount;
    }

    //Destroy window
    SDL_DestroyWindow( window );

    //Quit SDL subsystems
    SDL_Quit();
}

const Color &Window::getBgCol() const {
    return bgCol;
}

void Window::setBgCol(const Color &c) {
    bgCol = c;
}

void Window::loadPainting(Screen &s) {

    // Color
    int r, g, b, a;

    for (int y = 0; y < s.getHeight(); ++y) {
        for (int x = 0; x < s.getWidth(); ++x) {
            s.getPxl(x, y).to256(r, g, b, a);
            SDL_SetRenderDrawColor( renderer, r, g, b, a);
            SDL_RenderDrawPoint(renderer, x, y);
        }
    }
    bgCol.to256(r, g, b, a);
    SDL_SetRenderDrawColor( renderer, r, g, b, a);
}

void Window::forceQuitWindow() {
    forceQuit = true;
}

bool Window::isInitialized() {
    return initialized;
}

void Window::setMoving(bool moving) {
    movingConstants = moving;
}

bool Window::getMoving() {
    return movingConstants;
}

void Window::setSave(bool _save) {
    savePic = _save;
}
