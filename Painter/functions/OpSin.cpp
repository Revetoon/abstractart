//
// Created by revetoon on 2/17/20.
//

#include "OpSin.hh"

double OpSin::func() {
    double retVal = std::sin(v->func());
    assert(retVal >= -1 && retVal <= 1);
    return retVal;
}

void OpSin::print(std::ostream &out) {
    out << "sin(";
    v->print(out);
    out << ")";
}

void OpSin::build() {
    v = tracker.getOperator();
}

OpSin::OpSin(Tracker &tracker) : OpGeneric(tracker)
{
    build();
}

