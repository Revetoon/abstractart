//
// Created by revetoon on 2/17/20.
//

#include "OpConstant.hh"

double OpConstant::func() {
    return *v;
}

void OpConstant::print(std::ostream &out) {
    out << *v;
}

void OpConstant::build() {
    v = tracker.getConstant();
}

OpConstant::OpConstant(Tracker &tracker) : OpGeneric(tracker)
{
    build();
}