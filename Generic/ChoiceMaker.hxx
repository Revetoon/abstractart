//
// Created by revetoon on 2/18/20.
//

#ifndef ABSTRACTART_CHOICEMAKER_HXX
#define ABSTRACTART_CHOICEMAKER_HXX

#include <random>
#include <cassert>
#include <iomanip>
#include "ChoiceMaker.hh"

template <class T>
ChoiceMaker<T>::ChoiceMaker(std::initializer_list<Choice> list) {
    needRecompute = true;
    for (const auto &c : list) {
        choices.push_back({
                                  idTracker,
                                  c.value,
                                  c.weight,
                                  0.0
                          });
        ++idTracker;
    }
}

template<class T>
int ChoiceMaker<T>::addChoice(const ChoiceMaker::Choice &c) {
    needRecompute = true;
    choices.push_back({
                              idTracker,
                              c.value,
                              c.weight,
                              0.0
                      });
    return idTracker++;
}

template <class T>
int ChoiceMaker<T>::addChoice(int value, double weight) {
    needRecompute = true;
    choices.push_back({
                              idTracker,
                              value,
                              weight,
                              0.0
                      });
    return idTracker++;
}

template <class T>
T ChoiceMaker<T>::choose() {
    if (needRecompute)
        computeProbabilities();

    std::random_device dev;
    std::mt19937 rng(dev());
    static std::uniform_real_distribution<> rnd(0, 10);
    double val = rnd(rng) / 10.0;

    double probSum = 0.0;
    for (const auto &c : choices) {
        probSum += c.probability;
        if (val <= probSum)
            return c.value;
    }
    assert(false && "Should not happen since all probabilities should have been tested.");
}

template<class T>
void ChoiceMaker<T>::computeProbabilities() {
    needRecompute = false;

    double sum = 0;
    for (const auto &c : choices)
        sum += c.weight;

    for (auto &c : choices)
        c.probability = c.weight / sum;

}

template<class T>
const std::vector<typename ChoiceMaker<T>::ChoiceProba> &ChoiceMaker<T>::getChoices() {
    if (needRecompute)
        computeProbabilities();
    return choices;
}

template<class T>
void ChoiceMaker<T>::setWeight(int id, double weight) {
    needRecompute = true;
    choices[id].weight = weight;
}

template<class T>
void ChoiceMaker<T>::print(std::ostream &out) {
    if (needRecompute)
        computeProbabilities();
    out << "Choices: " << std::endl;
    out << "| ID    | Val   | Wgt   | prob  |" << std::endl;
    for (const auto &c : choices) {
        out <<  "| " << std::setfill(' ') << std::setw(5) << c.id \
            << " | " << std::setfill(' ') << std::setw(5) << c.value \
            << " | " << std::setfill(' ') << std::setw(5) << c.weight \
            << " | " << std::setfill(' ') << std::setw(5) << c.probability << " |" << std::endl;
    }
}



#endif //ABSTRACTART_CHOICEMAKER_HXX
