//
// Created by revetoon on 2/18/20.
//

#ifndef ABSTRACTART_OPPOW3_HH
#define ABSTRACTART_OPPOW3_HH


#include "OpGeneric.hh"

class OpPow3 : public OpGeneric {
public:
    OpPow3(Tracker &tracker);

    void build() override;

    double func() override;

    void print(std::ostream &out) override;

public:

    std::shared_ptr<OpGeneric> v = nullptr;
};


#endif //ABSTRACTART_OPPOW3_HH
