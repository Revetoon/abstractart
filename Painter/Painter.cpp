//
// Created by revetoon on 2/17/20.
//

#include <cassert>
#include <random>
#include "Painter.hh"


Painter::Painter()
    :tracker(Tracker(PAINTER_DEFAULT_MAX_RECURSE, {&_x, &_y}))
{}


Painter::Painter(int maxRecurse)
    :tracker(Tracker(maxRecurse, {&_x, &_y}))
{
    generateFormula();
}

void Painter::printFormula(std::ostream &out) const {
    formula->print(out);
    std::cout << std::endl;
}

double Painter::solve(double x, double y) {
    _x.value = x;
    _y.value = y;
    return formula->func();
}

void Painter::generateFormula(int _maxRecurse) {
    tracker.maxRecurse = _maxRecurse;
    formula = tracker.getOperator();
}

void Painter::paint(Screen &screen) {
    for (int i = 0; i < 3; ++i) {
        paintColorEnum col = static_cast<paintColorEnum>(i);
        paintOne(screen, col);
    }
}

#ifdef PROGRAM_MULTITHREAD
void Painter::paintThreaded(Screen &screen) {

    pthread_t threads[3]; // one thread for each color
    ThreadDataPaintColor threadData[3];
    int rc;

    for (int i = 0; i < 3; ++i) {
        paintColorEnum col = static_cast<paintColorEnum>(i);
        threadData[i].p = this;
        threadData[i].scr = &screen;
        threadData[i].col = col;
        rc = pthread_create(&threads[i], nullptr, paintOneThreaded, (void*) &threadData[i]);

        if (rc) {
            std::cout << "Error: Unable to create thread, " << rc << std::endl;
        }
    }

    pthread_exit(nullptr);
}

void *Painter::paintOneThreaded(void *data) {
    ThreadDataPaintColor *dataConv = (ThreadDataPaintColor *) data;
    dataConv->p->paintOne(*dataConv->scr, dataConv->col);

    pthread_exit(nullptr);
}
#endif // PROGRAM_MULTITHREAD



void Painter::paintOne(Screen &screen, Painter::paintColorEnum color) {
    double xNorm, yNorm;

    for (int y = 0; y < screen.getHeight(); ++y) {
        for (int x = 0; x < screen.getWidth(); ++x) {

            // set x and y variables to pixel coord
            xNorm = static_cast<double>(x) / static_cast<double>(screen.getWidth()) * 2.0 - 1.0;
            yNorm = static_cast<double>(y) / static_cast<double>(screen.getHeight()) * 2.0 - 1.0;
            _x.value = xNorm;
            _y.value = yNorm;

            // calculate result for current pixel
            double val = formula->func();
            val = (val + 1.0) / 2.0; // from [-1, 1] to [0, 1]

            // add resulting color to screen
            if (color == paintColorEnum::RED) {
                setConstantsR();
                screen.setPxlR(x, y, val);
            }
            else if (color == paintColorEnum::GREEN) {
                setConstantsG();
                screen.setPxlG(x, y, val);
            }
            else if (color == paintColorEnum::BLUE) {
                setConstantsB();
                screen.setPxlB(x, y, val);
            }
        }
    }
}



void Painter::randomizeConstants() {
    std::random_device dev;
    std::mt19937 rng(dev());
    static std::uniform_real_distribution<> rnd(-1, 1);

    for (int i = 0; i < tracker.constants.size(); ++i) {
        double r = rnd(rng);
        tracker.constants[i] = r;
        tracker.constantsR[i] = r;
        tracker.constantsG[i] = rnd(rng);
        tracker.constantsB[i] = rnd(rng);
    }
}

void Painter::incrementConstants(float incr) {

    for (int i = 0; i < tracker.constants.size(); ++i) {
        tracker.constants[i] += incr;
        if (tracker.constants[i] > 1.0) {
            tracker.constants[i] -= 2.0;
        }
        tracker.constantsR[i] += incr;
        if (tracker.constantsR[i] > 1.0) {
            tracker.constantsR[i] -= 2.0;
        }
        tracker.constantsG[i] += incr;
        if (tracker.constantsG[i] > 1.0) {
            tracker.constantsG[i] -= 2.0;
        }
        tracker.constantsB[i] += incr;
        if (tracker.constantsB[i] > 1.0) {
            tracker.constantsB[i] -= 2.0;
        }
    }
//    for (double & constant : tracker.constants) {
//        constant += incr;
//        if (constant > 1.0) {
//            constant -= 2.0;
//        }
//    }
}

void Painter::setConstantsR() {
    for (int i = 0; i < tracker.constants.size(); ++i) {
        tracker.constants[i] = tracker.constantsR[i];
    }
}

void Painter::setConstantsG() {
    for (int i = 0; i < tracker.constants.size(); ++i) {
        tracker.constants[i] = tracker.constantsG[i];
    }
}

void Painter::setConstantsB() {
    for (int i = 0; i < tracker.constants.size(); ++i) {
        tracker.constants[i] = tracker.constantsB[i];
    }
}
