//
// Created by revetoon on 2/17/20.
//

#include "OpNeg.hh"

void OpNeg::build() {
    v = tracker.getOperator();
}

double OpNeg::func() {
    double retVal = -v->func();
    assert(retVal >= -1 && retVal <= 1);
    return retVal;
}

void OpNeg::print(std::ostream &out) {
    out << "-";
    v->print(out);
}

OpNeg::OpNeg(Tracker &tracker) : OpGeneric(tracker)
{
    build();
}
