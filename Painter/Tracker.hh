//
// Created by revetoon on 2/17/20.
//

#ifndef ABSTRACTART_TRACKER_HH
#define ABSTRACTART_TRACKER_HH

#include <vector>
#include <memory>

#include "Variable.hh"
#include "../Generic/ChoiceMakerProgressive.hh"

class OpGeneric; // forward declaration

class Tracker {
public:
    enum OpName : uint32_t {
        OP_ABS,
        OP_AVG,
        OP_BELL,
        OP_CONSTANT,
        OP_COS,
        OP_EUCLIDIAN,
        OP_IFSUP,
        OP_MOD,
        OP_MULT,
        OP_NEG,
        OP_PLUS,
        OP_POW2,
        OP_POW3,
        OP_ROUND,
        OP_SIN,
        OP_VAR,
    };


public:
    Tracker(int maxRecurse, std::initializer_list<Variable *> vars = {});

    std::shared_ptr<OpGeneric> getOperator();
    Variable *getVariable();
    double *getConstant();

    void randomizeConstants();


public:
    int maxRecurse;
    ChoiceMakerProgressive<OpName> cm;
    std::vector<double> constants = std::vector<double>();

    // constants for R G and B
    std::vector<double> constantsR = std::vector<double>();
    std::vector<double> constantsG = std::vector<double>();
    std::vector<double> constantsB = std::vector<double>();
    std::vector<Variable *> variables = std::vector<Variable *>();

    int count = 0;
};

#endif //ABSTRACTART_TRACKER_HH
