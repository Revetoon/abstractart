//
// Created by revetoon on 2/17/20.
//

#include "OpRound.hh"

void OpRound::build() {
    v = tracker.getOperator();
    p = tracker.getOperator();
}

double OpRound::func() {
    double val = p->func();
    if (val < 0)
        val = -val;
    if (val == 0)
        return 0;
    val *= 10;
    double r = static_cast<double>(static_cast<int>(v->func() * val) / val);
    if (r > 1)
        r -= 2.0;
    else if (r < -1)
        r += 2.0;
    double retVal = r;
    assert(retVal >= -1 && retVal <= 1);
    return retVal;
}

void OpRound::print(std::ostream &out) {
    out << "round(";
    v->print(out);
    out << ", ";
    p->print(out);
    out << ")";
}

OpRound::OpRound(Tracker &tracker) : OpGeneric(tracker)
{
    build();
}
