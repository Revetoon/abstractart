//
// Created by revetoon on 2/16/20.
//

#ifndef ABSTRACTART_COLOR_HH
#define ABSTRACTART_COLOR_HH


class Color {
public:

    Color();
    Color(int gray);
    Color(int r, int g, int b);
    Color(int r, int g, int b, int a);
    Color(double gray);
    Color(double r, double g, double b);
    Color(double r, double g, double b, double a);

    void to256(int &rOut, int &gOut, int &bOut);
    void to256(int &rOut, int &gOut, int &bOut, int &aOut);


    // DEFAULT COLORS
public:
    static Color White() { return Color(1.0, 1.0, 1.0); };
    static Color Black() { return Color(0.0, 0.0, 0.0); };
    static Color Red() { return Color(1.0, 0.0, 0.0); };
    static Color Green() { return Color(0.0, 1.0, 0.0); };
    static Color Blue() { return Color(0.0, 0.0, 1.0); };

public:
    double r = 0.0;
    double g = 0.0;
    double b = 0.0;
    double a = 1.0;
};


#endif //ABSTRACTART_COLOR_HH
