//
// Created by revetoon on 2/17/20.
//

#ifndef ABSTRACTART_OPIFSUP_HH
#define ABSTRACTART_OPIFSUP_HH


#include "OpGeneric.hh"

class OpIfSup : public OpGeneric {
public:
    OpIfSup(Tracker &tracker);

public:
    void build() override;

    double func() override;

    void print(std::ostream &out) override;

private:
    std::shared_ptr<OpGeneric> a;
    std::shared_ptr<OpGeneric> b;
    std::shared_ptr<OpGeneric> yes;
    std::shared_ptr<OpGeneric> no;
};


#endif //ABSTRACTART_OPIFSUP_HH
