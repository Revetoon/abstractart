//
// Created by revetoon on 2/17/20.
//

#ifndef ABSTRACTART_OPSIN_HH
#define ABSTRACTART_OPSIN_HH


#include "OpGeneric.hh"

class OpSin : public OpGeneric {

public:
    OpSin(Tracker &tracker);

    double func() override;

    void build() override;

    void print(std::ostream &out) override;

public:

    std::shared_ptr<OpGeneric> v = nullptr;

};


#endif //ABSTRACTART_OPSIN_HH
