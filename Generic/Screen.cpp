//
// Created by revetoon on 2/17/20.
//

#include <stdexcept>
#include "Screen.hh"
#include "../External/Jpge/jpge.h"

Screen::Screen()
    : width(0), height(0), initialized(true)
{
    data = std::vector<Color>();
}

Screen::Screen(int width, int height)
    : width(width), height(height), initialized(false)
{
    data = std::vector<Color>(width * height);
}

void Screen::init(int _width, int _height) {
    width = _width;
    height = _height;
    initialized = true;
    data = std::vector<Color>(width * height);
}

void Screen::setPxl(int x, int y, const Color &c) {
    *val(x, y) = c;
}

Color Screen::getPxl(int x, int y) const {
    return Color(*val(x, y));
}


Color *Screen::val(int x, int y) {
    if (x >= width || y >= height)
        throw std::out_of_range("Trying to access a value outside of screen bounds.");
    return &data[y * width + x];
}

const Color *Screen::val(int x, int y) const {
    if (x >= width || y >= height)
        throw std::out_of_range("Trying to access a value outside of screen bounds.");
    return &data[y * width + x];
}

int Screen::getWidth() const {
    return width;
}

int Screen::getHeight() const {
    return height;
}

void Screen::setPxlR(int x, int y, const double &r) {
    val(x, y)->r = r;
}

void Screen::setPxlG(int x, int y, const double &g) {
    val(x, y)->g = g;
}

void Screen::setPxlB(int x, int y, const double &b) {
    val(x, y)->b = b;
}

bool Screen::save(const char *path) {

    uint8_t *pxls = new uint8_t[width * height * 3];

    int i = 0;
    int j = 0;

    for (; i < width * height * 3; i += 3, j += 1) {
        pxls[i]   = data[j].r * 255;
        pxls[i+1] = data[j].g * 255;
        pxls[i+2] = data[j].b * 255;
    }

    return jpge::compress_image_to_jpeg_file(path, width, height, 3, pxls);
}

bool Screen::isInitialized() const {
    return initialized;
}

