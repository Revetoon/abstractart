//
// Created by revetoon on 2/17/20.
//

#ifndef ABSTRACTART_OPPLUS_HH
#define ABSTRACTART_OPPLUS_HH


#include "OpGeneric.hh"

class OpPlus : public OpGeneric {
public:
    OpPlus(Tracker &tracker);

public:
    void build() override;

    double func() override;

    void print(std::ostream &out) override;

public:

    std::shared_ptr<OpGeneric> a;
    std::shared_ptr<OpGeneric> b;
};


#endif //ABSTRACTART_OPPLUS_HH
