//
// Created by revetoon on 2/20/20.
//

#ifndef ABSTRACTART_OPAVG_HH
#define ABSTRACTART_OPAVG_HH


#include "OpGeneric.hh"

class OpAvg : public OpGeneric {
public:
    OpAvg(Tracker &tracker);

public:
    void build() override;

    double func() override;

    void print(std::ostream &out) override;


private:
    std::shared_ptr<OpGeneric> a;
    std::shared_ptr<OpGeneric> b;
};


#endif //ABSTRACTART_OPAVG_HH
