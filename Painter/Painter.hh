//
// Created by revetoon on 2/17/20.
//

#ifndef ABSTRACTART_PAINTER_HH
#define ABSTRACTART_PAINTER_HH

#include "functions/Operators.hh"
#include "Tracker.hh"
#include "../Generic/Screen.hh"
#include "../Properties.hh"

#define PAINTER_DEFAULT_MAX_RECURSE         10

class Painter {
    enum paintColorEnum  : uint8_t {
        RED,
        GREEN,
        BLUE
    };

    struct ThreadDataPaintColor {
        int thread_id;
        Painter *p;
        Screen * scr;
        paintColorEnum col;
    };

public:

    static Painter & getInstance() {
        static Painter instance;

        return instance;
    }

    Painter();

private:
    Painter(int maxRecurse);

public:
    void generateFormula(int _maxRecurse = PAINTER_DEFAULT_MAX_RECURSE);

    void paint(Screen &screen);
    void paintOne(Screen &screen, paintColorEnum color);

#ifdef PROGRAM_MULTITHREAD
    // multithreaded
    void paintThreaded(Screen &screen);
    static void *paintOneThreaded(void *data);
#endif // PROGRAM_MULTITHREAD

    void printFormula(std::ostream &out) const;
    double solve(double x, double y);

    void randomizeConstants();

    void incrementConstants(float incr = 0.1);

    void setConstantsR();
    void setConstantsG();
    void setConstantsB();

    Painter(Painter const&)               = delete;
    void operator=(Painter const&)        = delete;

private:

    Variable _x = {0.0, "X"};
    Variable _y = {0.0, "Y"};

    Tracker tracker;

    std::shared_ptr<OpGeneric> formula;

};


#endif //ABSTRACTART_PAINTER_HH
