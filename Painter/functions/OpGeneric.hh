//
// Created by revetoon on 2/17/20.
//

#ifndef ABSTRACTART_OPGENERIC_HH
#define ABSTRACTART_OPGENERIC_HH

#include <cmath>
#include <iostream>
#include <memory>
#include <cassert>
#include "../Tracker.hh"

class OpGeneric {

public:

    // build arguments
    virtual void build() = 0;

    // executes the function
    virtual double func() = 0;

    // pretty print
    virtual void print(std::ostream &out) = 0;

protected:

    explicit OpGeneric(Tracker &tracker) : tracker(tracker) {};

public:

    Tracker &tracker;

};


#endif //ABSTRACTART_OPGENERIC_HH
