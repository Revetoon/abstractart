//
// Created by revetoon on 2/17/20.
//

#include "OpPlus.hh"

void OpPlus::build() {
    a = tracker.getOperator();
    b = tracker.getOperator();
}

double OpPlus::func() {
    double r = a->func() + b->func();
    if (r > 1)
        r -= 2.0;
    else if (r < -1)
        r += 2.0;
    double retVal = r;
    assert(retVal >= -1 && retVal <= 1);
    return retVal;
}

void OpPlus::print(std::ostream &out) {
    a->print(out);
    out << " + ";
    b->print(out);
}

OpPlus::OpPlus(Tracker &tracker) : OpGeneric(tracker)
{
    build();
}
